import 'package:flutter/material.dart';

class FirstOpenModel {
  String img;
  String text;
  String desc;
  Color bg;
  Color button;

  FirstOpenModel({
    required this.img,
    required this.text,
    required this.desc,
    required this.bg,
    required this.button,
  });
}
