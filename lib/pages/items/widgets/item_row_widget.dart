import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../core/constants/theme.dart';

class ItemRowWidget extends StatelessWidget {
  const ItemRowWidget(
      {required this.title,
      required this.price,
      required this.rate,
      super.key});
  final String title;
  final double rate;
  final double price;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        SizedBox(
          width: 130,
          child: Text(
            title,
            style: TextStyle(
                fontFamily: GoogleFonts.inter.toString(),
                fontWeight: FontWeight.w500,
                fontSize: 22),
            overflow: TextOverflow.ellipsis,
            softWrap: true,
          ),
        ),
        const SizedBox(width: 8),
        const Icon(
          Icons.star_border,
          color: Color(0xFF5B79F1),
          size: 17,
        ),
        const SizedBox(width: 2),
        Text("$rate",
            style: TextStyle(
              fontFamily: GoogleFonts.inter.toString(),
              fontWeight: FontWeight.w500,
              fontSize: 14,
            )),
        const Spacer(),
        Text("\$$price",
            style: TextStyle(
              color: ThemeColors.primary,
              fontFamily: GoogleFonts.inter.toString(),
              fontWeight: FontWeight.w500,
              fontSize: 22,
            )),
      ],
    );
  }
}
