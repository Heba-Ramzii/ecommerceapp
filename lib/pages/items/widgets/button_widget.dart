import 'package:ecommerce_app/core/constants/theme.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ButtonWidget extends StatelessWidget {
  const ButtonWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 52,
      width: 136,
      decoration: BoxDecoration(
          color: ThemeColors.primary, borderRadius: BorderRadius.circular(60)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          IconButton(
            onPressed: () {},
            icon: const Icon(Icons.remove_circle_outline_rounded),
            color: Colors.white,
            iconSize: 20,
          ),
          Text(
            "1",
            style: TextStyle(
                color: Colors.white,
                fontFamily: GoogleFonts.inter.toString(),
                fontSize: 18,
                fontWeight: FontWeight.w500),
          ),
          IconButton(
            onPressed: () {},
            icon: const Icon(Icons.add_circle_outline_rounded),
            color: Colors.white,
            iconSize: 20,
          ),
        ],
      ),
    );
  }
}
