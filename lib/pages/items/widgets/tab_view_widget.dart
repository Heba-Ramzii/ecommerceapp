import 'package:ecommerce_app/core/constants/theme.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class TabViewWidget extends StatelessWidget {
  const TabViewWidget({required this.tabviewController, super.key});

  final TabController tabviewController;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 30,
      width: double.infinity,
      child: TabBar(
        controller: tabviewController,
        labelPadding: EdgeInsets.zero,
        labelColor: ThemeColors.primary,
        labelStyle: TextStyle(
          fontSize: 16,
          fontFamily: GoogleFonts.inter.toString(),
          fontWeight: FontWeight.w400,
        ),
        unselectedLabelColor: Colors.black,
        unselectedLabelStyle: TextStyle(
          fontSize: 16,
          fontFamily: GoogleFonts.inter.toString(),
          fontWeight: FontWeight.w400,
        ),
        indicatorColor: ThemeColors.primary,
        tabs: const [
          Tab(
            child: Text(
              "Description",
            ),
          ),
          Tab(
            child: Text(
              "Reviews",
            ),
          ),
          Tab(
            child: Text(
              "Offers",
            ),
          ),
          Tab(
            child: Text(
              "Policy",
            ),
          ),
        ],
      ),
    );
  }
}
