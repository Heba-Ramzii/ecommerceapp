import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import '../../../core/constants/theme.dart';
import '../widgets/button_widget.dart';
import '../widgets/item_row_widget.dart';
import '../widgets/slider_widget.dart';
import '../widgets/tab_view_widget.dart';
import '../widgets/tabbare_view_widget.dart';

class ItemViewPage extends StatefulWidget {
  const ItemViewPage({super.key});

  @override
  State<ItemViewPage> createState() => _ItemViewPageState();
}

class _ItemViewPageState extends State<ItemViewPage>
    with TickerProviderStateMixin {
  late TabController tabviewController;
  Future<ItemResponseDetails>? itemRD;
  @override
  void initState() {
    super.initState();
    tabviewController = TabController(length: 4, vsync: this);
    itemRD = DataResponse.request();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: const Icon(
            Icons.arrow_back,
            size: 30,
          ),
          title: Text('Universe',
              style: TextStyle(
                  fontFamily: GoogleFonts.roboto.toString(),
                  fontSize: 24,
                  fontWeight: FontWeight.w600)),
          actions: const [
            Padding(
                padding: EdgeInsets.only(right: 18),
                child: Icon(Icons.favorite_border))
          ],
        ),
        body: FutureBuilder(
            future: itemRD,
            builder: (context, snapshot) {
              return Padding(
                padding: const EdgeInsets.only(left: 18, right: 18),
                child: Column(
                  children: [
                    const SizedBox(height: 51),
                    SliderWidget(
                      image: snapshot.data?.image ?? "",
                    ),
                    const SizedBox(height: 53),
                    ItemRowWidget(
                      title: snapshot.data?.title ?? '',
                      rate: snapshot.data?.rating?.rate ?? 0.0,
                      price: snapshot.data?.price ?? 0.0,
                    ),
                    const SizedBox(height: 53),
                    TabViewWidget(tabviewController: tabviewController),
                    TabBareViewWidget(
                      tabviewController: tabviewController,
                      description: snapshot.data?.description ?? '',
                    ),
                    const SizedBox(height: 47),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        const ButtonWidget(),
                        Container(
                          height: 52,
                          width: 163,
                          decoration: BoxDecoration(
                              color: ThemeColors.primary,
                              borderRadius: BorderRadius.circular(60)),
                          child: TextButton(
                            onPressed: () {},
                            child: Text(
                              "ADD TO CART",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontFamily: GoogleFonts.inter.toString(),
                                  fontSize: 18),
                            ),
                          ),
                        )
                      ],
                    )
                  ],
                ),
              );
            }));
  }
}

class ItemResponseDetails {
  int? id;
  String? image;
  String? title;
  Rating? rating;
  double? price;
  String? description;

  ItemResponseDetails(
      {required this.id,
      required this.title,
      required this.image,
      required this.rating,
      required this.description,
      required this.price});

  ItemResponseDetails.fromJson(Map<String, dynamic> json) {
    id = json["id"];
    image = json["image"];
    title = json["title"];
    rating = Rating.fromJson(json["rating"]);
    price = json["price"];
    description = json["description"];
  }
}

class Rating {
  double? rate;
  int? count;

  Rating(this.rate, this.count);
  Rating.fromJson(Map<String, dynamic> json) {
    rate = json["rate"];
    count = json["count"];
  }
}

class DataResponse {
  static Future<ItemResponseDetails> request() async {
    Response<Map<String, dynamic>> response =
        await Dio().get('https://fakestoreapi.com/products/1',
            options: Options(
              responseType: ResponseType.json, // Specify response type
            ));

    if (response.statusCode == 200) {
      return ItemResponseDetails.fromJson(response.data!);
    } else {
      throw Exception('Failed to load album');
    }
  }
}
