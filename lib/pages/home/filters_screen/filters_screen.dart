import 'package:flutter/material.dart';

import '../home.dart';

class FiltersScreen extends StatefulWidget {
  const FiltersScreen({super.key});


  @override
  State<FiltersScreen> createState() => _FiltersScreenState();
}

class _FiltersScreenState extends State<FiltersScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
    body: Padding(
      padding: const EdgeInsets.symmetric(horizontal: 18),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 30.0),
            child: Row(
              children: [
                IconButton(
                    onPressed:  (){
                      Navigator.of(context).pop();
                    },
                    icon: const Icon(Icons.close)
                ),
                const SizedBox(width: 5,),
                const Text(
                    'Filters',
                  style: TextStyle(
                    fontSize: 18
                  ),
                )
              ],
            ),
          ),
          const Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Categories',
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Expanded(
                child: Divider(
                  indent: 20,
                  endIndent: 20,
                 ),
              ),
             ],
          ),
          buildRadioListCat(),
          const Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Brand',
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Expanded(
                child: Divider(
                  indent: 20,
                  endIndent: 20,
                 ),
              ),
             ],
          ),
          buildRadioListBrand(),
          Container(
            height: 52,
            width: 200,
            decoration:  BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              gradient: const LinearGradient(
                begin: Alignment.topRight,
                end: Alignment.bottomLeft,
                colors: [
                  Color(0xff003366),
                  Color(0xff003366),
                  Color(0x802c6dad),
                ],
              ),
            ),
            child: InkWell(
                onTap:  (){
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const HomeScreen()));
                },
              child: const Center(
                child: Text(
                    'APPLY FILTER',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 18
                  ),
                ),
              ),

            ),
          ),

        ],
      ),
    ),
    );
  }
  var _selectedCategory;
  var _selectedBrand;

  final List<String> _categories = [
    'Beds',
    'Cabiets',
    'Chairs And Seating',
    'Chests',
    'Clocks',
  ];
  final List<String> _brand = [
    'Roche Bobois',
    'Roche Bobois',
    'Roche Bobois',
    'Roche Bobois',
    'Roche Bobois',
  ];

  Widget buildRadioListCat() {
    List<Widget> list = [];
    for (int i = 0; i < _categories.length; i++) {
       list.add(
        RadioListTile(
          value: i,
          groupValue: _selectedCategory,
          title: Text(_categories[i]),
          onChanged: (val) {
            setState(() {
              _selectedCategory = val!;
            });
          },
        ),
      );
    }
    return Column(
      children: list,
    );
  }

  Widget buildRadioListBrand() {
    List<Widget> listb = [];
    for (int i = 0; i < _brand.length; i++) {
      listb.add(
        RadioListTile(
          value: i,
          groupValue: _selectedBrand,
          title: Text(_brand[i]),
          onChanged: (value) {
            setState(() {
              _selectedBrand = value!;
            });
          },
        ),
      );
    }
    return Column(
      children: listb,
    );
  }
}
