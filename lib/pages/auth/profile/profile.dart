// ignore_for_file: must_be_immutable

import 'package:ecommerce_app/core/constants/theme.dart';

import 'package:flutter/material.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ThemeColors.primary,
      body: SafeArea(
          minimum: const EdgeInsets.symmetric(horizontal: 18, vertical: 54),
          child: Column(
            children: [
              const ProfileAppBar(),
              // Profile Image and UserName  &  Email
              Padding(
                padding: const EdgeInsets.only(top: 60, bottom: 60),
                child: SizedBox(
                  child: Column(
                    children: [
                      const CircleAvatar(
                        radius: 60,
                        backgroundColor: Colors.white,
                        // child: Image.asset(''),
                      ),
                      const Padding(
                        padding: EdgeInsets.only(bottom: 5, top: 20),
                        child: Text('Abdelmajeed Asil',
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w600,
                              fontSize: 18,
                            )),
                      ),
                      Text('abdelmajeedasil@gmail.com',
                          style: TextStyle(
                            color: Colors.white.withOpacity(.7),
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                          )),
                    ],
                  ),
                ),
              ),
              const ProfileBody()
            ],
          )),
    );
  }
}

class ProfileAppBar extends StatelessWidget {
  const ProfileAppBar({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 50,
      width: double.infinity,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              IconButton(
                  onPressed: () {},
                  icon: const Icon(
                    Icons.arrow_back,
                    color: Colors.white,
                  )),
              Text(
                'My Account',
                style: TextStyle(
                    color: Colors.white.withOpacity(.7),
                    fontWeight: FontWeight.w400,
                    fontSize: 18),
              )
            ],
          ),
          ElevatedButton(
            style: const ButtonStyle(
                backgroundColor: MaterialStatePropertyAll(Colors.white)),
            onPressed: () {},
            child: const Text(
              'Edit TO Profile',
              style: TextStyle(
                  color: ThemeColors.primary,
                  fontSize: 9,
                  fontWeight: FontWeight.w500),
            ),
          )
        ],
      ),
    );
  }
}

class ProfileBody extends StatelessWidget {
  const ProfileBody({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      height: MediaQuery.of(context).size.height * .47,
      // height: 420,
      padding: const EdgeInsets.all(20),
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(25)),
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            ProfileSettingButtens(
              settingTitle: 'Upcoming Orders',
              icon: const Icon(
                Icons.shopping_bag,
                color: Color(0xfff8492AD),
              ),
            ),
            ProfileSettingButtens(
              settingTitle: 'Manage Address',
              icon: const Icon(
                Icons.location_on,
                color: Color(0xfff8492AD),
              ),
            ),
            ProfileSettingButtens(
              settingTitle: 'Update payment',
              icon: const Icon(
                Icons.money,
                color: Color(0xfff8492AD),
              ),
            ),
            ProfileSettingButtens(
              settingTitle: 'My Chats',
              icon: const Icon(
                Icons.chat_rounded,
                color: Color(0xfff8492AD),
              ),
            ),
            ProfileSettingButtens(
              settingTitle: 'Logout',
              icon: const Icon(
                Icons.login,
                color: Color(0xfff8492AD),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ProfileSettingButtens extends StatelessWidget {
  ProfileSettingButtens(
      {super.key, required this.icon, required this.settingTitle});
  String settingTitle;
  Icon icon;
  @override
  Widget build(BuildContext context) {
    return Container(
      // height: 57,
      height: MediaQuery.of(context).size.height * .06,
      decoration: const BoxDecoration(
        color: Color(0xFFFF3F3F5),
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      child: Padding(
        padding: const EdgeInsets.only(left: 20.0),
        child: Row(children: [
          icon,
          const SizedBox(
            width: 20,
          ),
          Text(
            settingTitle,
            style: TextStyle(
              color: Colors.black.withOpacity(.8),
              fontWeight: FontWeight.w700,
              fontSize: 16,
            ),
          )
        ]),
      ),
    );
  }
}
