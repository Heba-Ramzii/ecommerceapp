import 'package:ecommerce_app/core/constants/theme.dart';
import 'package:ecommerce_app/pages/auth/signup/signup.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final formKey = GlobalKey<FormState>();

  final emailController = TextEditingController();

  final passwordController = TextEditingController();

  bool isPassword= true;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Padding(
            padding: const EdgeInsets.only(left: 40.0,right: 40,bottom: 85),
            child: Form(
              key: formKey,
              child:  Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [

                  ///Header Of Login
                  const SizedBox(
                    height: 53,
                  ),
                  //todo: put logo of app
                  const CircleAvatar(
                    radius: 50,
                    backgroundColor: ThemeColors.primary,
                  ),
                  const SizedBox(
                    height: 46,
                  ),
                  const Text("Login",

                      style: TextStyle(
                        color: Color.fromRGBO(0, 51, 102, 1),
                        fontWeight: FontWeight.w600,
                        fontSize: 35,)),
                  const SizedBox(
                    height: 13,
                  ),
                  const Text("Login to continue using the app",
                      style: TextStyle(
                        color: Color.fromRGBO(0, 51, 102, 0.6),
                        fontWeight: FontWeight.w400,
                        fontSize: 14,
                      )),

                  ///login with social
                  const SizedBox(
                    height: 43,
                  ),
                  //todo: put icons of social login
                  const Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CircleAvatar(
                        radius: 25,
                        backgroundColor: ThemeColors.primary,
                      ),
                      SizedBox(
                        width: 25,
                      ),
                      CircleAvatar(
                        radius: 25,
                        backgroundColor: ThemeColors.primary,
                      ),
                      SizedBox(
                        width: 25,
                      ),
                      CircleAvatar (
                        radius: 25,
                        backgroundColor: ThemeColors.primary,
                      ),

                    ],
                  ),
                  const SizedBox(
                    height: 21,
                  ),
                  const Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Expanded(child: Divider(
                          indent: 1,
                          endIndent: 20,
                          thickness: 2,
                          color: Color.fromRGBO(0, 0, 0, 0.5)
                      )),
                      Text("or",
                          style: TextStyle(
                              color: Color.fromRGBO(0, 0, 0, 0.9),
                              fontWeight: FontWeight.w500,
                              fontSize: 16
                          )),
                      Expanded(child: Divider(
                          thickness: 2,
                          indent: 20,
                          endIndent: 1,
                          color: Color.fromRGBO(0, 0, 0, 0.5)
                      ))
                    ],
                  ),

                  ///inputs of login
                  const SizedBox(
                    height: 54,
                  ),
                  TextFormField(
                      controller: emailController,
                      keyboardType: TextInputType.emailAddress,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return "Please Enter Your Email";
                        } else {
                          return null;
                        }
                      },
                    decoration: const InputDecoration(
                      fillColor: Color.fromRGBO(243, 243, 245, 1),
                      filled: true,
                      floatingLabelBehavior: FloatingLabelBehavior.never,
                      // hintText: hintText,
                      // hintStyle: hintStyle,
                      labelText: "Email",
                      labelStyle: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        color: Color.fromRGBO(170, 178, 199, 1)
                      ),
                      errorBorder: OutlineInputBorder(
                          borderSide: BorderSide.none),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide.none),
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide.none),
                       border: OutlineInputBorder(
                           borderSide:BorderSide.none ),

                      prefixIcon: Icon(
                        Icons.person_2_outlined,
                         color: Color.fromRGBO(119, 132, 166, 1),
                      ),


                    ),
                  ),
                  const SizedBox(
                    height: 18,
                  ),
                  TextFormField(
                    controller: passwordController,
                    keyboardType: TextInputType.visiblePassword,
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Please Enter Your Password";
                      } else {
                        return null;
                      }
                    },
                    //todo
                    obscureText: isPassword?true:false,

                    decoration:  InputDecoration(
                      fillColor: const Color.fromRGBO(243, 243, 245, 1),
                      filled: true,
                      floatingLabelBehavior: FloatingLabelBehavior.never,
                      // hintText: hintText,
                      // hintStyle: hintStyle,
                      labelText: "••••••••••",
                      labelStyle: const TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          color: Color.fromRGBO(170, 178, 199, 1)
                      ),
                      errorBorder: const OutlineInputBorder(
                          borderSide: BorderSide.none),
                      enabledBorder: const OutlineInputBorder(
                          borderSide: BorderSide.none),
                      focusedBorder: const OutlineInputBorder(
                          borderSide: BorderSide.none),
                      border: const OutlineInputBorder(
                          borderSide:BorderSide.none ),
                      prefixIcon: const Icon(Icons.lock_outline,
                        color: Color.fromRGBO(119, 132, 166, 1),
                      ),
                      //todo
                      suffixIcon:  IconButton(
                        icon: Icon(
                            isPassword ?Icons.visibility_off:
                            Icons.visibility_rounded),
                        onPressed: (){
                          setState(() {
                            isPassword=!isPassword;
                          });
                        },
                      ),



                    ),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Row(
                    children: [
                      const Spacer(),
                      InkWell(
                        onTap: () {
                          // Navigator.push(context, MaterialPageRoute(builder: (context) => ForgetPasswordScreen(),));
                        },
                        child: const Text(
                          "Forget Password?",
                          style: TextStyle(
                              fontSize: 11,
                              fontWeight: FontWeight.w400,
                            color: Color.fromRGBO(0, 51, 102, 0.8),
                              decoration: TextDecoration.underline
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 42,
                  ),
                  Container(
                    width: double.infinity,
                    height: 60,
                    decoration: BoxDecoration(
                        color:const Color.fromRGBO(0, 51, 102, 1),
                        borderRadius: BorderRadius.circular(10)
                    ),

                    child: MaterialButton(
                      onPressed: (){
                        if(formKey.currentState!.validate()){
                          ///todo:navigation to home
                        }
                      },
                      child: const Text(
                        "Login",
                        style: TextStyle(
                            fontWeight: FontWeight.w300,
                            color: Colors.white, fontSize: 18 ),
                      ),
                    ),
                  ),
                  const SizedBox(height: 87,),

                  ///do not have account
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text(
                        "Don’t have an account? ",
                        style: TextStyle(
                            fontSize: 11,
                            color: Color.fromRGBO(0, 0, 0, 1),
                            fontWeight: FontWeight.w500),
                      ),
                      InkWell(
                          onTap: () {
                           Navigator.of(context).push(MaterialPageRoute(builder: (context) => SignUpScreen(),));
                          },
                          child: const Text(
                            "Register",
                            style: TextStyle(
                                fontSize: 11,
                                fontWeight: FontWeight.w400,
                                color: Color.fromRGBO(0, 51, 102, 1)),
                          ))
                    ],
                  ),



                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
