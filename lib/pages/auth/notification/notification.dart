import 'package:flutter/material.dart';

class NotificationScreen extends StatelessWidget {
  const NotificationScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 18),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 30.0),
              child: Row(
                children: [
                  IconButton(
                      onPressed:  (){
                        Navigator.of(context).pop();
                      },
                      icon: const Icon(Icons.close)
                  ),
                  const SizedBox(width: 5,),
                  const Text(
                    'Notification',
                    style: TextStyle(
                        fontSize: 18
                    ),
                  )
                ],
              ),
            ),
            Expanded(
              child: ListView.separated(
                physics: const BouncingScrollPhysics(),
                 itemCount: 8,
               itemBuilder: (BuildContext context, int index) {
                 return Container(
                      height:   57,
                 decoration:   BoxDecoration(
                   borderRadius: BorderRadius.circular(5),
                   color: const Color( 0xffF3F3F5),
                 ),
                   child: const Padding(
                     padding: EdgeInsets.symmetric(horizontal: 20.0),
                     child: Row(
                       mainAxisAlignment: MainAxisAlignment.end,
                       children: [
                         Text(
                             'Hello Now You Have Offer!',
                           style: TextStyle(
                             color: Color(0xff003366),
                             fontSize: 14,

                           ),
                         ),
                         Spacer(),
                         Text(
                             '03:37 PM',
                           style: TextStyle(
                             color: Color( 0xff707070),
                             fontSize: 14,
                           ),
                         ),
                       ],
                     ),
                   ),
                 );
               },
                separatorBuilder: (context,state)=>const SizedBox(height: 8.0,),
              
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 20.0),
              child: Container(
                height: 52,
                width: 200,
                decoration:  BoxDecoration(
                  borderRadius: BorderRadius.circular(50),
                  gradient: const LinearGradient(
                    begin: Alignment.topRight,
                    end: Alignment.bottomLeft,
                    colors: [
                      Color(0xff003366),
                      Color(0xff003366),
                      Color(0x802c6dad),
                    ],
                  ),
                ),
                        child: InkWell(
                          onTap:  (){
                          },
                          child: const Center(
                            child: Text(
                              'clear all',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 18
                              ),
                    ),
                  ),

                ),
              ),
            ),
          ],
        ),
      ),

    );
  }
}
