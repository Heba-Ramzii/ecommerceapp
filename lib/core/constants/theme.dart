import 'package:flutter/material.dart';

class ThemeColors {
  static const primary = Color(0xFF003366);
  static const accent = Color(0xFFD3D3D3);
  static const secondary = Color(0xFFFFA07A);
}
